const jwt = require('jsonwebtoken');
const config = require('../../../env.json');

const auth = (req, res, next) => {
  const token = req.header('auth-token');
  if (!token) return res.status(401).send('Acceso Denegado');

  try {
    const verified = jwt.verify(token, config.service.jwt_key);
    req.user = verified;
    next();

  } catch (error) {
    next(error);
  }
}

module.exports = auth;
