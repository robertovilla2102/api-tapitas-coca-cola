const controller = {};
const { User } = require('../models');

const config = require('../../env.json');
const jwt = require('jsonwebtoken');

controller.register = async (req, res, next) => {
  const { fullName, email, password } = req.body;

  try {
    // chequeamos que el email no esté registrado en la db
    const checkEmail = await User.findOne({ email });
    if (checkEmail) return res.status(400).json({ message: 'Email no disponible, utilice uno nuevo.' });

    // creamos el user y hasheamos su password
    const newUser = new User({ fullName, email, password, method: 'local' });
    newUser.password = await newUser.encryptPassword(password);

    const savedUser = await newUser.save();

    res.status(200).json(savedUser);
  } catch(err){
    next(err);
  }
};

controller.login = async (req, res, next) => {
  const { email, password } = req.body;

  try {
    // check email in db
    const user = await User.findOne({ email });
    if (!user) return res.status(400).send('Email is not found');

    // check password
    const validPassword = await user.matchPassword(password);
    if (!validPassword) return res.status(400).send('incorrect password');

    // create and assign jwt
    const token = jwt.sign({ _id: user._id }, config.service.jwt_key)

    res.header('auth-token', token).status(200).json(token);
  } catch (err) {
    next(err);
  }
}

controller.edit = async (req, res, next) => {
  const { id } = req.user;
  const { fullName, email } = req.body;

  try {
    const userUpdated = await User.findByIdAndUpdate(id, { email, fullName }, { new: true });
    res.status(200).send(userUpdated);
  } catch (err) {
    next(err);
  }
}




module.exports = controller;
