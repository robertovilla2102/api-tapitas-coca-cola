const { Schema, model } = require('../../config/database');
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
  fullName: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  misCanjes: [{
    type: Schema.Types.ObjectId, ref: 'canjeados'
  }],
  method: {
    type: String,
    default: 'local'
  }
})

UserSchema.methods.encryptPassword = async password => {
  const salt = await bcrypt.genSalt(10)
  return await bcrypt.hash(password, salt)
}

UserSchema.methods.matchPassword = async function (password) {
  const resu = await bcrypt.compare(password, this.password)
  return resu
}

module.exports = model('users', UserSchema);
