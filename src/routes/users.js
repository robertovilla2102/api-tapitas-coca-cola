const router = require('express').Router();
const UsuarioController = require('../controllers/userControllers');

//* jwt and validations
const verify = require('../middlewares/tokens/index');

// const validate = require('../middlewares/validations')
/* validate(editValdiation),  ---------- esto iria en la ruta*/ 
// const {
//   loginValidations,
//   registerValidation,
//   editValdiation,
//   editPassword
// } = require('../middlewares/validations/userValidations')

//* Trae los cupones favoritos de un user
// router.get('/favorites', verifyJWT, UsuarioController.favorites);

//* Registrar a un usuario de forma local
router.post('/register', UsuarioController.register);

//* Loguea a un usuario local
router.post('/login', UsuarioController.login);

//* Edita los datos del usuario, no incluye la contraseña
router.put('/edit', verify, UsuarioController.edit);

//* Permite recuperar la contraseña desde la app nativa y loguea al usuario
// router.post('/recover-password', UsuarioController.recoverPassword);

// //* Envia un email al user, con un codigo (Para resetear la password)
// router.post('/send-code', UsuarioController.sendResetCode);

// //* Chequea que el resetCode y el codigo que manda el user (Para resetear la password), coincidan
// router.post('/check-code', UsuarioController.checkResetCode);

// //* Desloguea a un usuario
// router.get('/logout', verifyJWT, UsuarioController.logout);

module.exports = router;
