const router = require('express').Router();

router.get('/', async(req, res, next) => {
  try {
    res.status(200).send('API its working!');
  }
  catch(err){
    next(err);
  }
})

router.get('/time', async(req, res, next) => {
  try {
    const time = new Date();
    res.status(200).send(time);
  }
  catch(err){
    next(err);
  }
})

module.exports = router;