const router = require('express').Router();

//* import routes
const Test = require('./test');
const User = require('./users');

//* set routes
router.use('/', Test);
router.use('/user', User);


module.exports = router;