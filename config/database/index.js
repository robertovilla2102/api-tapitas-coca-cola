const mongoose = require('mongoose');
const config = require('../../env.json');

mongoose.connect(config.service.db_key, {
  useNewUrlParser: true,
  useFindAndModify: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}, () => {
  return console.log('conected to db')
});

module.exports = mongoose;
