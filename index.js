const express = require('express');
const app = express();

const config = require('./env.json');
const morgan = require('morgan');
const cors = require('cors');

//* middlewares and config
app.use(cors({ origin: true }))
app.use(morgan('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//* conectando db
require('./config/database');

//* set and config port
app.set('port', config.service.port || 3030)

//* API rotues
app.use('/api', require('./src/routes'));
app.use('/', (req, res) => res.redirect('/api'));

//* error middleware 
app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something broke!');
});

//* server on
app.listen(app.get('port'), () => {
  console.log('Server on port: ', app.get('port'))
});